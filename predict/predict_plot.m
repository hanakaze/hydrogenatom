doavg = input('do average?');
donorm = input('do normalize?');

load('FCorrectHatomCE_densityfunction_1maxQN_10.mat')
load('lifetime.mat');

load('CE_pred18.mat')


N=10; %data range 
M

n = [2:N];
CE = sum(sum(S,3),2);
CE = CE(2:end);

CE_pred = sum(sum(S_pred,3),2);
CE_pred = CE_pred(M);


lft = sum(Aexact1,1);
lft = lft';

lft_pred = lft(M-1);
lft = lft(1:N-1);
lft = lft/10^8;
lft_pred = lft_pred/10^8;

if doavg == 1
    aCE = CE./n'./n';
    aCE_pred = CE_pred./M./M;
    alft = lft./(n-1)';
    alft_pred = lft_pred./(M-1)';
else 
    alft = lft;
    aCE = CE;
    alft_pred = lft_pred;
    aCE_pred = CE_pred;
end

if donorm == 1
    aCE = aCE/norm(aCE,2);
    alft = alft/norm(alft,2);
    aCE_pred = aCE_pred/norm(aCE_pred,2);
    alft_pred = alft_pred/norm(alft_pred,2);
end

 save('Rdata18.mat','aCE','aCE_pred','alft','alft_pred')

[pf,Er] = polyfit(log(aCE),log(alft),1)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%plot method 2: loglog(aCE,alft)
loglog(aCE,alft,'d','MarkerSize',15,'MarkerFaceColor','black');
hold on;

loglog(aCE_pred, exp(pf(2))*aCE_pred.^pf(1),'MarkerSize',15,'MarkerFaceColor','yellow');
loglog(aCE_pred,alft_pred,'MarkerSize',15,'MarkerFaceColor','red')
% loglog(aCE,exp(pf(2))*aCE.^pf(1),'-.','Li)neWidth',4); %fit line