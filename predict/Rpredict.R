library(ggplot2)


logdata <- log(data)
model <- lm(logdata, formula = alft~aCE)

datapred <- data.frame(aCEpred = data$aCEpred, alftpred = data$alftpred)[1,]
logdatapred <- log(datapred)

newdata <- predict(model,data.frame(aCE = logdatapred$aCEpred))

ggplot(logdata,aes(x=aCE,y=alft)) + geom_point()+stat_smooth(fullrange = TRUE,se=TRUE)+
  geom_point(mapping = aes(x=logdatapred$aCEpred,y=newdata))