%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% wave function and fourier transform function definition
generate_wave_fun = @(n, l, r) factorial(n+l)*((2.0*(1/n))^(l+1)/factorial(n+l))*sqrt((1/n)*factorial(n-l-1)/(n*factorial(n+l)))*(exp(-(1/n)*r)).*(r.^l).*(laguerreL(n-l-1,2*l+1,2.0*(1/n)*r));

fourier_transform = @(l,R,r,k,x) ((-1)^l)*((sqrt(k)'*ones(1,length(r))).^(-1).*(besselj(l+0.5,k'*r))*(R.*r.*r./sqrt(r))');

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
N_qN = input('max principle quantum number=');

isdensityfunction = input('Calculate density function?');
S = zeros(N_qN,N_qN,2*N_qN-1);  % dimension (n,l,m)
R_interval= [50,100,200,300,400,500,600,800,1000,900,900,900,900,900,900,800,800,800];
    n_qN = N_qN;
   for l_qN = 0:1:n_qN-1
        for m_qN = -l_qN:1:l_qN
            %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
            % coordinate initializer       
            N_c = 2000;
            R = zeros(1,N_c);
            r_interval = R_interval(l_qN+1);
            hr = r_interval/N_c;
            r = linspace(hr, r_interval, N_c);

            %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
            % momentum initializer
            hk = 0.001;
            k_interval = 2.5/l_qN;
%             N_k = floor(k_interval/hk);
            N_k = 2000;
            k = linspace(hk, k_interval, N_k);
            N_x = 200;
            hx = 2/(N_x-1);
            x = linspace(-1, 1, N_x);
            F_k = zeros(N_k,N_x);
            f_k = zeros(N_k,N_x);
            CE_density = zeros(N_k,N_x);
            %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
            if(isdensityfunction)
                % use density function
                %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
                % expansion of density function on spherical harmonics
                syms xx;
                P2_order =2*l_qN;
                P_coeff = zeros(P2_order+1,P2_order+1);
                for i=1:1:P2_order+1
                    Pi_coeff = coeffs(legendreP(i-1,xx));
                    Pi_len = length(Pi_coeff);
                    for j=1:1:Pi_len
                        P_coeff(i,i-2*(j-1)) = Pi_coeff(Pi_len+1-j);
                    end
                end

                P2_coeff = zeros(1,P2_order+1);
                
                if m_qN < 0
                    m_qN = -m_qN;
                    Plm = (-1)^(m_qN)*(1-xx^2)^(m_qN/2)*diff(legendreP(l_qN,xx),m_qN,xx);
                    Plm = (-1)^(m_qN)*(factorial(l_qN-m_qN)/factorial(l_qN+m_qN))*Plm;
                    m_qN =-m_qN;
                else
                    Plm = (-1)^(m_qN)*(1-xx^2)^(m_qN/2)*diff(legendreP(l_qN,xx),m_qN,xx);
                end
                
                P2i_coeff = coeffs(Plm^2);
                P2i_len = length(P2i_coeff);
                for j=1:1:P2i_len
                    P2_coeff(P2_order+1-2*(j-1)) = P2i_coeff(P2i_len+1-j);
                end

                P2_coeff_on_p = inv(transpose(P_coeff))*P2_coeff';
                %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
                % density function
                R = generate_wave_fun(n_qN, l_qN, r);
                R = R.*conj(R); % density function input 

                for l_qN_i = 0:2:P2_order
                    F_k = F_k + sqrt(2*l_qN_i+1)*P2_coeff_on_p(l_qN_i+1)*(fourier_transform(l_qN_i, R, r, k)*legendreP(l_qN,x));
                end
            %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
            % use wavefunction
            else 
                R = generate_wave_fun(n_qN, l_qN, r);
                legendre_lm = legendre(l_qN,x);
                
                F_k = fourier_transform(l_qN, R, r, k)*legendre_lm(abs(m_qN)+1,:);
            end

            %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
            % FT
            f_k = F_k.*conj(F_k);
            f_k = f_k/max(max(f_k));
            CE_density = -f_k.*log(f_k).*((k'.^2)*ones(1,N_x));
            CE_density(f_k==0) =0.0;
            CE = 2*pi*hk*hx*sum(sum(CE_density)); 
            S(n_qN,l_qN+1,m_qN+n_qN) = CE;
        end
    end


 S_av = sum(S,3);
for j=1:1:N_qN
    S_av(:,j) = S_av(:,j)/(2*j-1);
end

L_qN = [0:N_qN-1];
save(strcat('prediction_',num2str(isdensityfunction),'maxQN_',num2str(N_qN),'.mat'),'L_qN','S','S_av');
S_pred = S;
M = N_qN;
save(strcat('CE_pred',num2str(M),'.mat'),'M','S_pred');

 
       