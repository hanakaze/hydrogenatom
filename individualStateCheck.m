
p3 = S_av(3,1);
p2 = S_av(2,2);
s3 = S_av(3,1);
s2 = S_av(2,1);
s1 = S_av(1,1);

h1 = struct('states','2p-1s','lft',6.25,'dCE',p2-s1);
h2 = struct('states','3s-2p','lft',0.063,'dCE',s3-p2);
h3= struct('states','3p-1s','lft',1.64,'dCE',p3-s1);
h4 = struct('states','3p-3s','lft',0.22,'dCE',p3-s2);


hatom = cell(4,1);

hatom = {h1,h2,h3,h4};
lft = zeros(4,1);
dCE = zeros(4,1);
    
figure;
hold on;
for i=1:4
    scatter(hatom{i}.lft,hatom{i}.dCE);
    text(hatom{i}.lft+0.5,hatom{i}.dCE,hatom{i}.states)
    lft(i) = hatom{i}.lft;
    dCE(i) = hatom{i}.dCE;
end

