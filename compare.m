% scheme: sum A and average, sum CE and divide multiplicity, use initial state
% as A, start state n=2
% CE is over-suppressed, modified to CE/n^2, lft/n
load('lifetime.mat');
N=10;
n = [2:N];
CE = sum(sum(S,3),2);
CE = CE(2:end);

M = 200;

lft = sum(Aexact1,1);
lft = lft';
lft = lft(1:N-1);

% 
% aCE = CE./n'./n';
% alft = lft./(n-1)';
alft = lft;
aCE = CE;

% aCE = aCE/norm(aCE,2);
% alft = alft/norm(alft,2);

% aCE = aCE/max(aCE);
% alft = alft/max(alft);
[pf,Er] = polyfit(log(aCE),log(alft),1)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%plot method 2: loglog(aCE,alft)
loglog(aCE,alft,'d','MarkerSize',10,'MarkerFaceColor','black');
hold on;

loglog(aCE,exp(pf(2))*aCE.^pf(1),'-.','LineWidth',2); %fit line

b = num2str(n'); c = cellstr(b);
text(aCE, 1.5*alft, c, 'FontSize',12); %add label to each point

hold off;

xlabel('CE');
ylabel('1/lifetime');
% xlabel('Normalized CE');
% ylabel('Normalized 1/lifetime');

set(gcf,'Position',[0,0,800,800],'Color','w');
set(gca,'LineWidth',2,'FontSize',16);
legend('denotes principal quantum number','Best fit')
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% % plot method 1: (n,aCE) and (n,alft) in one pic
% yyaxis left;
% loglog(n,aCE,'-','Marker','<','LineWidth',2,'MarkerSize',10,'MarkerFaceColor','black');
% yyaxis right;
% loglog(n,alft,'-.','Marker','d','LineWidth',2,'MarkerSize',10,'MarkerFaceColor','black');
% set(gca,'XScale','linear')
% legend('(normalized) CE','(normalized) 1/life time of spontaneous emission')
% legend('CE','1/life time of spontaneous emission')
% nax = axes('position',[0.1,0.9,0.3,0.3]);
% plot(aCE,'d','MarkerSize',10,'MarkerFaceColor','black');
% hold on;
% xlabel('principle quantum number $n$','interpreter','latex');
% set(gcf,'Position',[0,0,800,800],'Color','w');
% set(gca,'LineWidth',2,'FontSize',16);
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% figure;
% pf = polyfit(alft',aCE,1)
% y = polyval(pf,alft);
% plot(alft,aCE,'o','MarkerSize',6,'MarkerFaceColor','black');
% hold on;
% xlabel('(normalized) life time of spontaneous emission');
% ylabel('CE');
% plot(alft,y,'-.','LineWidth',2);
% legend('CE','Fitted CE using linear regression')
% nax = axes('position',[0.1,0.9,0.3,0.3]);
% plot(alft(2:end-1),aCE(2:end-1),'o','MarkerSize',6,'MarkerFaceColor','black');
% hold on;
% plot(alft(2:end-1),y(2:end-1),'-.','LineWidth',2);
% set(gcf,'Position',[0,0,800,800],'Color','w');
% set(gca,'LineWidth',2,'FontSize',16);
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
