% scheme: sum A and average, sum CE and divide multiplicity, use initial state
% as A, start state n=2
% CE is over-suppressed, modified to CE/n^2, lft/n
doavg = input('do average?');
donorm = input('do normalize?');

load('lifetime.mat');
N=10;
n = [2:N];
CE = sum(sum(S,3),2);
CE = CE(2:end);

M = 200;

lft = sum(Aexact1,1);
lft = lft';
lft = lft(1:N-1);
lft = lft/10^8;

if doavg == 1
    aCE = CE./n'./n';
    alft = lft./(n-1)';
else 
    alft = lft;
    aCE = CE;
end

if donorm == 1
    aCE = aCE/norm(aCE,2);
    alft = alft/norm(alft,2);
end

% aCE = aCE/max(aCE);
% alft = alft/max(alft);
[pf,Er] = polyfit(log(aCE),log(alft),1)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%plot method 2: loglog(aCE,alft)
loglog(aCE,alft,'d','MarkerSize',15,'MarkerFaceColor','black');
hold on;

loglog(aCE,exp(pf(2))*aCE.^pf(1),'-.','LineWidth',4); %fit line

b = num2str(n'); c = cellstr(b);
text(aCE, 1.5*alft, c, 'FontSize',14); %add label to each point

hold off;

if donorm == 0 & doavg == 0
    xlabel('CE (a_0^3)');
    ylabel('1/lifetime (10^8/s)');
    xlim([0.08,6.0]);
elseif donorm == 0 & doavg == 1
    xlabel('Avaraged CE (a_0^3)');
    ylabel('Averaged 1/lifetime (10^8/s)');
    xlim([0.0007,2.0]);
else
    xlabel('Normalized CE');
    ylabel('Normalized 1/lifetime');
    xlim([0.0006,2.0]);
end

set(gcf,'Position',[0,0,800,800],'Color','w');
set(gca,'LineWidth',2,'FontSize',16);
legend('denotes principal quantum number','Best fit');
savefig(strcat('xCEyLFT_avg',num2str(doavg),'_','norm',num2str(donorm),'.fig'));
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% plot method 1: (n,aCE) and (n,alft) in one pic
% yyaxis left;
% loglog(n,aCE,'-','Marker','<','LineWidth',2,'MarkerSize',10,'MarkerFaceColor','black');
% yyaxis right;
% loglog(n,alft,'-.','Marker','d','LineWidth',2,'MarkerSize',10,'MarkerFaceColor','black');
% set(gca,'XScale','linear')
% 
% if donorm == 0 & doavg == 0
%     legend('CE','1/lifetime');
% elseif donorm == 0 & doavg == 1
%     legend('(averaged) CE','(averaged) 1/lifetime');
% else
%     legend('(normalized) CE','(normalized) 1/lifetime');
% end
% 
% xlabel('principal quantum number $n$','interpreter','latex');
% set(gcf,'Position',[0,0,800,800],'Color','w');
% set(gca,'LineWidth',2,'FontSize',16);
% savefig(strcat('xNyCELFT_avg',num2str(doavg),'_','norm',num2str(donorm),'.fig'));
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% plot method 1: (n,aCE) and (n,alft) in one pic, single axis
% loglog(n,aCE,'-','Marker','<','LineWidth',2,'MarkerSize',10,'MarkerFaceColor','black');
% hold on;
% loglog(n,alft,'-.','Marker','d','LineWidth',2,'MarkerSize',10,'MarkerFaceColor','black');
% set(gca,'XScale','linear')
% 
% if donorm == 0 & doavg == 0
%     legend('CE (a_0^3)','1/lifetime (10^8/s)');
% elseif donorm == 0 & doavg == 1
%     legend('(averaged) CE (a_0^3)','(averaged) 1/lifetime (10^8/s)');
% else
%     legend('(normalized) CE','(normalized) 1/lifetime');
% end
% 
% xlabel('principal quantum number $n$','interpreter','latex');
% set(gcf,'Position',[0,0,800,800],'Color','w');
% set(gca,'LineWidth',2,'FontSize',16);
% savefig(strcat('xNyCELFT_avg',num2str(doavg),'_','norm',num2str(donorm),'.fig'));
% hold off;
% 
