% scheme: sum A with multiplicity, sum CE without average, use final state
% as A, start state n=2

load('lifetime.mat');
N=10;
n = [1:N];
CE = sum(sum(S,3),2);

M = 200;
n = [2:M].*[2:M];
nsq = ones(N,1)*n;

lft = sum(Aexact1(1:N,1:M-1).*nsq,2);
CE = CE(1:N);

% lft = sum(Aexact,2);
% lft = lft';

lft = lft(1:N)./[M-1:-1:M-N]';
aCE = CE/norm(CE,2);
alft = lft/norm(lft,2);

% aCE = aCE/max(aCE);
% alft = alft/max(alft);

[pf,E] = polyfit(log(alft),log(aCE),1)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
loglog(aCE,'d','MarkerSize',10,'MarkerFaceColor','black');
hold on;
loglog(alft,'-.','LineWidth',2);
legend('(normalized) CE','(normalized) 1/life time of spontaneous emission')
% hold off;
% nax = axes('position',[0.1,0.9,0.3,0.3]);
% plot(aCE,'d','MarkerSize',10,'MarkerFaceColor','black');
% hold on;
% plot(alft,'-.','LineWidth',2);
set(gcf,'Position',[0,0,800,800],'Color','w');
set(gca,'LineWidth',2,'FontSize',16);
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% figure;
% pf = polyfit(alft',aCE,1)
% y = polyval(pf,alft);
% plot(alft,aCE,'o','MarkerSize',6,'MarkerFaceColor','black');
% hold on;
% xlabel('(normalized) life time of spontaneous emission');
% ylabel('CE');
% plot(alft,y,'-.','LineWidth',2);
% legend('CE','Fitted CE using linear regression')
% nax = axes('position',[0.1,0.9,0.3,0.3]);
% plot(alft(2:end-1),aCE(2:end-1),'o','MarkerSize',6,'MarkerFaceColor','black');
% hold on;
% plot(alft(2:end-1),y(2:end-1),'-.','LineWidth',2);
% set(gcf,'Position',[0,0,800,800],'Color','w');
% set(gca,'LineWidth',2,'FontSize',16);
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
